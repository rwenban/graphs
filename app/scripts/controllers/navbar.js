/**
 * Created by factornine on 10/03/2014.
 */

'use strict';

angular.module('graphsApp')
    .controller('NavbarCtrl', function ($scope, $location) {

        $scope.menu = [
            {
                'title': 'Home',
                'link': '/'
            }

        ];

        $scope.isActive = function (route) {
            return route === $location.path();
        };
    });
